package com.example.ejspinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private Spinner spOpciones;
    private TextView tvOpciones, tvOp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cargarViews();
        String[] opciones = {
                "Madrid",
                "Barcelona",
                "Cadiz",
                "Almeria",
                "Sevilla"
        };
        Arrays.sort(opciones);

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, opciones);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.layout_spinner, opciones);
        spOpciones.setAdapter(adapter);
        ponerListener();
    }

    //----------------------------------------------------------------------------------------------

    private void cargarViews() {
        spOpciones = (Spinner) findViewById(R.id.spOpciones);
        tvOpciones = (TextView) findViewById(R.id.tvOpciones);
        tvOp = (TextView) findViewById(R.id.tvOp);
    }

    //----------------------------------------------------------------------------------------------

    public void verOpcion(View v){
        String seleccion = spOpciones.getSelectedItem().toString();
        tvOpciones.setText(seleccion);
    }

    //----------------------------------------------------------------------------------------------

    public void ponerListener(){
        spOpciones.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tvOp.setText(spOpciones.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
